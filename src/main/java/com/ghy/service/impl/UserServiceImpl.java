package com.ghy.service.impl;

import com.ghy.dao.UserDao;
import com.ghy.entity.User;
import com.ghy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    public List<User> query() {
        return userDao.findAll();
    }

    public User save(User user) {
        return userDao.save(user);
    }
}
