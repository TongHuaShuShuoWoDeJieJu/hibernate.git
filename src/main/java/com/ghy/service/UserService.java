package com.ghy.service;


import com.ghy.entity.User;

import java.util.List;

public interface UserService {
    public List<User> query();

    public User save(User user);
}
