package com.ghy.test;

import com.ghy.entity.User;
import com.ghy.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SpringDataJpaTest {
    @Autowired
    private UserService userService;

    @Test
    void contextLoads() {
        List<User> list = userService.query();
        for (User user : list) {
            System.out.println(user);
        }
    }
}
